import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { EditableField } from './EditableField';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box
          textAlign="center"
          mt={2}
          display="flex"
          flexDirection="column"
          gap="12px"
        >
          <EditableField
            initialValue={name}
            onConfirm={console.log}
            onValidation={(v) => v.length >= 4}
          />
          <EditableField
            initialValue={email}
            onConfirm={console.log}
            onValidation={(v) => v.length >= 4}
          />
        </Box>
      </Box>
    </Card>
  );
};
