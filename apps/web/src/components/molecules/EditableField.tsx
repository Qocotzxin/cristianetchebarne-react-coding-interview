import { Box, TextField } from '@mui/material';
import { useState, type FC, type ChangeEvent } from 'react';

interface EditableFieldProps {
  initialValue: string;
  onConfirm: (value: string) => void;
  onValidation?: (value: string) => boolean;
}

export const EditableField: FC<EditableFieldProps> = ({
  initialValue,
  onConfirm,
  onValidation,
}) => {
  const [value, setValue] = useState(initialValue);
  const [isEditing, setIsEditing] = useState(false);
  const [isValid, setIsValid] = useState(true);

  const onConfirmHandler = () => {
    onConfirm(value);
    setIsEditing(false);
  };

  const onCancelHandler = () => {
    setIsEditing(false);
    setValue(initialValue);
  };

  const onChangeHandler = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    if (onValidation) {
      setIsValid(onValidation(e.target.value));
    }
    setValue(e.target.value);
  };

  return !isEditing ? (
    <button onClick={() => setIsEditing(true)}>{value}</button>
  ) : (
    <Box
      sx={{
        display: 'flex',
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end',
      }}
    >
      <TextField
        sx={{ width: '100%' }}
        id="outlined-basic"
        label="Outlined"
        variant="outlined"
        value={value}
        onChange={onChangeHandler}
      />
      <Box
        sx={{
          width: 'fit-content',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          gap: '12px',
        }}
      >
        <button disabled={!isValid} onClick={onConfirmHandler}>
          Confirm
        </button>
        <button onClick={onCancelHandler}>Cancel</button>
      </Box>
    </Box>
  );
};
